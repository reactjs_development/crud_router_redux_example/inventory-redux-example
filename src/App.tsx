import React from 'react';
import logo from './logo.svg';
import './App.css';
import Routers from './components/routers/routers';
import { Provider } from 'react-redux';
import configureStore from './components/store/configureStore';
const state = configureStore({});

function App() {
  return (

    // here we using Provider in main component because we can access stored value using redux in all child component
    <Provider store={state}>
      <div className="App">
      <Routers/>
    </div>
    </Provider>
    
  );
}

export default App;
