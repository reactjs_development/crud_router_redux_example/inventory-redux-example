import {
  GET_INVENTORY_STOCK,
  SALE_MEN_INVENTORY_STOCK,
  SALE_INVENTORY_STOCK,
  REORDER_INVENTORY_STOCK,
} from "./actionTypes";

export const getInventory = () => {
  return {
    type: GET_INVENTORY_STOCK,
  };
};

// export const incrementNumber = ( number:number) => {
// return {
//     type : SALE_MEN_INVENTORY_STOCK,
//     salNum :number
// }

// }

export const saleInventory = (productKey: string, number: number) => {
  return {
    type: SALE_INVENTORY_STOCK,
    productKey: productKey,
    orderCount: number,
  };
};

export const reorderInventory = (productKey: string, number: number) => {
  return {
    type: REORDER_INVENTORY_STOCK,
    productKey: productKey,
    orderCount: number,
  };
};
