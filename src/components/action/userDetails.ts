import axios from "axios";
import {
  FETCH_USER_DETAILS_ERROR,
  FETCH_USER_DETAILS_START,
  FETCH_USER_DETAILS_SUCCESS,
} from "./actionTypes";

const url = "http://jsonplaceholder.typicode.com/users";

export const getUserDetails = () => {
  return (dipatch: any) => {
    dipatch({ type: FETCH_USER_DETAILS_START });
    return axios
      .get(url)
      .then((response) => dipatch({ type: FETCH_USER_DETAILS_SUCCESS, user : response.data }))
      .catch((err) => {
        dipatch({ type: FETCH_USER_DETAILS_ERROR, err });
      });
  };
};
