import React from "react";
import {NavLink} from 'react-router-dom';
import './navBar.css'

export default function NavBar(){
    
return (

<div className="NavBar">
<NavLink  to='/'>Order View</NavLink>
<NavLink to='/orderManagement'>Order Mangement</NavLink>
<NavLink to='/addProductToInventory'>Add product to Inventory</NavLink>
<NavLink to='/userDetails'>User Details</NavLink>
<NavLink to='/fetchData'>Fetch Data</NavLink>
<NavLink to='/lazyLoading'>Lazy Loading</NavLink>
<NavLink to='/dragAndDrop'>Drag And Drop</NavLink>

</div>
);

}