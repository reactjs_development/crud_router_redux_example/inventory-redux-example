import {combineReducers} from 'redux' ;
import { inventoryReducers } from './inventoryReducers';
import { userReducers } from './userReducers';

export default combineReducers({
    inventory : inventoryReducers,
    users : userReducers
})