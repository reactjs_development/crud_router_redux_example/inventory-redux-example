import { GET_INVENTORY_STOCK, SALE_INVENTORY_STOCK,REORDER_INVENTORY_STOCK } from '../action/actionTypes';

const initialState:any = {
  menProduct:10,
  womenProduct:10,
};

export const inventoryReducers = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_INVENTORY_STOCK : return state;

    case SALE_INVENTORY_STOCK:
      return {
        ...state,
        [action.productKey]: state[action.productKey] - Number(action.orderCount),
      };

    case REORDER_INVENTORY_STOCK:
      return {
        ...state,
        [action.productKey]: state[action.productKey] + Number(action.orderCount),
      };

    default:
      return state;
  }
};
