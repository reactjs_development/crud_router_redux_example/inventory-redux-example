import {
  FETCH_USER_DETAILS_ERROR,
  FETCH_USER_DETAILS_START,
  FETCH_USER_DETAILS_SUCCESS,
} from "../action/actionTypes";

const initialState: any = {
  isLoading: false,
  isFailed: false,
  userDetails: undefined,
};

export const userReducers = (state = initialState, action: any) => {
  switch (action.type) {
    case FETCH_USER_DETAILS_START:
      return {
        ...state,
        isLoading: true,
        isFailed: false,
        userDetails: undefined,
       
      };

    case FETCH_USER_DETAILS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isFailed: false,
        userDetails: [...action.user],
      };

      case FETCH_USER_DETAILS_ERROR:
        return {
          ...state,
          isLoading: false,
        isFailed: true,
        userDetails: undefined,
        };
  
    default:
      return state;
  }
};
