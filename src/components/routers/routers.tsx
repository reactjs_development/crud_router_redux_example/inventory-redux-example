import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import OrderView from '../uiComponents/orderView/orderView';
import AddProductToInventory from '../uiComponents/addProductToInventory/addProductToInventory';
import OrderManagement from '../uiComponents/orderManagement/orderManagement';
import UserDetailsView from '../uiComponents/userDetailsView/userDetailsView';
import FetchDataFromApi from '../uiComponents/fetchData/fetchDataFromApi';
import NotFound from '../notFound/notFound';
import NavBar from '../navigation/navBar';
import LazyLoading from '../uiComponents/lazyLoading/lazyLoading';
import DragAndDrop from '../uiComponents/dragAndDrop/dragAndDrop';
import List from '../uiComponents/lazyLoading/lazyLoadingDiv';
function Routers() {

  return (
    <BrowserRouter>
      <NavBar />
      <Routes >
        <Route path="/" element={<OrderView />} />
        <Route path="orderManagement" element={<OrderManagement />} />
        <Route path="addProductToInventory" element={<AddProductToInventory />} />
        <Route path="userDetails" element={<UserDetailsView />} />
        <Route path="fetchData" element={<FetchDataFromApi />} />
        <Route path="lazyLoading" element={<List />} />
        <Route path='dragAndDrop' element={<DragAndDrop />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>

  );
}

export default Routers;
