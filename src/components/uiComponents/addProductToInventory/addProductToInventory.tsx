
import React, { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import './addProductToInventory.css'
import { reorderInventory,saleInventory } from "../../action/inventoryAction";

export default function AddProductToInventory() {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const [reorderProduct, setReorderProduct] = useState({
        menProduct: 0,
        womenProduct: 0,

    });

    const handleData = (e: any) => {
        setReorderProduct({
            ...reorderProduct, [e.target.name]: e.target.value
        })
        console.log("ReorderValue",{
            ...reorderProduct, [e.target.name]: e.target.value
        })
    }

    const updateProduct = () => {
        dispatch(reorderInventory('menProduct', reorderProduct.menProduct));
        dispatch(reorderInventory('womenProduct', reorderProduct.womenProduct));
        console.log("reorderInventory",
            reorderProduct.menProduct)
        navigate('/');

    }

    return (
        <div className="reorderProduct">
            <>
                <form className="reorderProduct-form" >
                    <label >Reorder men product</label><br />
                    <input className="reorderProduct-text"
                        type="number"
                        name="menProduct"
                        onChange={handleData}
                        value={reorderProduct.menProduct}
                    /><br />

                    <label>Reorder women product</label><br />
                    <input className="reorderProduct-text"
                        type="number"
                        name="womenProduct"
                        onChange={handleData}
                        value={reorderProduct.womenProduct}
                    />
                    <br />

                    <div className="button-layout">
                        <button className="reorderProduct-submit" onClick={updateProduct}>Reorder product</button>
                        <input className="reorderProduct-back" onClick={() => navigate('/')} type="submit" value="Back" />
                    </div>

                </form>

            </>

        </div>
    );

}