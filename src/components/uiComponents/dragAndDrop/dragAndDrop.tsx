import React, { useState } from "react";

interface Item {
  id: string;
  content: string;
}

interface DraggedItem {
  id: string;
  index: number;
}

export default function DragAndDrop() {
  const [items, setItems] = useState<Item[]>([
    { id: "1", content: "Item 1" },
    { id: "2", content: "Item 2" },
    { id: "3", content: "Item 3" },
    { id: "4", content: "Item 4" }
  ]);

  const [draggedItem, setDraggedItem] = useState<DraggedItem | null>(null);

  const handleDragStart = (event: React.DragEvent<HTMLDivElement>, index: number) => {
    setDraggedItem({ id: items[index].id, index });
    event.dataTransfer.effectAllowed = "move";
    event.dataTransfer.setData("text/html", event.currentTarget.innerHTML);
  };

  const handleDragOver = (event: React.DragEvent<HTMLDivElement>, index: number) => {
    event.preventDefault();
  };

  const handleDrop = (event: React.DragEvent<HTMLDivElement>, index: number) => {
    if (draggedItem === null) {
      return;
    }

    const newItems = [...items];
    const draggedIndex = draggedItem.index;
    const draggedItemContent = newItems[draggedIndex];

    newItems.splice(draggedIndex, 1);
    newItems.splice(index, 0, draggedItemContent);

    setItems(newItems);
    setDraggedItem(null);
  };

  return (
    <div className="DragAndDrop">
      <h1>Drag and Drop</h1>
      <div className="item-list">
        {items.map((item, index) => (
          <div
            key={item.id}
            className={`item ${draggedItem?.id === item.id ? "dragging" : ""}`}
            draggable
            onDragStart={(event) => handleDragStart(event, index)}
            onDragOver={(event) => handleDragOver(event, index)}
            onDrop={(event) => handleDrop(event, index)}
          >
            {item.content}
          </div>
        ))}
      </div>
    </div>
  );
}
