import React, { useState, useEffect } from "react";
import '../userDetailsView/userDetailsView.css'
import { useSelector, useDispatch } from "react-redux"
import { debounce } from 'lodash';

export default function FetchDataFromApi() {
 
    // 'data' is used to store the data fetched from the API.
    // 'currentPage' is used to store the current page number of the pagination.
    // 'usersPerPage' is used to store the number of users per page.
    // 'search' is used to store the value entered in the search input

    const [data, setData] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [usersPerPage, setUsersPerPage] = useState(3);
    const [search, setSearch] = useState("");
    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/users")
            .then((response) => response.json())
            .then((json) => setData(json));
    }, []);


    // The current users to display are calculated based on the current page number and the number of users per page.
    // The slice method is used to extract a portion of the data array to get the current users.
    const indexOfLastUser = currentPage * usersPerPage;
    const indexOfFirstUser = indexOfLastUser - usersPerPage;
    const currentUsers = data.slice(indexOfFirstUser, indexOfLastUser);

    //The handleChange function is used to handle the change in the number of users per page.
    // It updates the usersPerPage state and sets the current page back to 1.   
     const handleChange = (e: any) => {
        setUsersPerPage(e.target.value);
        setCurrentPage(1);
    };



    //The handleSearch function is used to handle the change in the search input. 
    //It updates the search state and sets the current page back to 1.    
    const handleSearch = debounce((e: any) => {
        setSearch(e.target.value);
        // setCurrentPage(1);
      }, 500);
      
    // Filtering the data based on the search input:
    const filteredData = currentUsers.filter((user: any) =>
        user.name.toLowerCase().includes(search.toLowerCase())
    );

    // based on page value we are updating  CurrentPage
    const handleClick = (page: number) => setCurrentPage(page);

    return (
        <div className="orderView">
            <div>
                <label>select number of data per page </label>
                <select value={usersPerPage} onChange={handleChange}>
                    <option value={3}>default</option>
                    <option value={2}>2</option>
                    <option value={5}>5</option>
                </select>
            </div>
            <div>
                <input
                    className="search-input"
                    type="text"
                    placeholder="Search by title"
                    value={search}
                    // onChange={(event) => setSearch(event.target.value) }
                    onChange={handleSearch}

                />
            </div>
            {filteredData &&
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>

                        </tr>
                    </thead>
                    {filteredData.map((user: any, index) => (
                        <tbody key={index}>
                            <tr >
                                <td>{user?.name}</td>
                                <td>{user?.email}</td>

                            </tr>
                        </tbody>
                    ))}

                </table>
            }
            <h3>{currentPage}-{currentUsers.length} of {data.length}</h3>
            <div className="btn">
                <button
                    className="pagination-button"
                    disabled={currentPage === 1}
                    onClick={() => handleClick(currentPage - 1)}
                >
                    {"<"}
                </button>
                <button
                    className="pagination-button"
                    disabled={currentPage === Math.ceil(data.length / usersPerPage)}
                    onClick={() => handleClick(currentPage + 1)}
                >
                    {">"}
                </button>
            </div>
        </div>
    );

}