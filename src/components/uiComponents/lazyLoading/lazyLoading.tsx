import React, { useState, useEffect } from "react";

interface ListItem {
  id: number;
  title: string;
  body: string;
}

function List() {
  const [listItems, setListItems] = useState<ListItem[]>([]);
  const [isFetching, setIsFetching] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);

  //(1)The hook calls the fetchData function to fetch the initial set of list items,
  // and attaches an event listener to the window object to listen for the scroll event

  //(2)The useEffect hook returns a cleanup function that removes the event listener when the component unmounts.
  useEffect(() => {
    fetchData();
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  //This defines a function handleScroll that checks whether the user has scrolled to the bottom of the page 
  //and whether the component is currently fetching more items. 
  //If the conditions are met, the isFetching state variable is set to true.

  // const handleScroll = () => {
  //   if (
  //     Math.ceil(window.innerHeight + document.documentElement.scrollTop) !==
  //     document.documentElement.offsetHeight ||
  //     isFetching
  //   )
  //     return;
  //   setIsFetching(true);
  // };

  const handleScroll = () => {
    const scrollPos = Math.ceil(window.innerHeight * 0.3);
    if (
      document.documentElement.scrollTop + scrollPos >= document.documentElement.offsetHeight ||
      isFetching
    )
      return;
    setIsFetching(true);
  };

  const fetchData = async () => {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=10`);
    const data: ListItem[] = await response.json();
    setPage(page + 1);
    setListItems(prevState => [...prevState, ...data]);
    setIsFetching(false);
  };

  //This sets up another useEffect hook that runs whenever the isFetching state variable changes. 
  //If isFetching is true, the fetchData function is called to fetch more data.
  useEffect(() => {
    if (!isFetching) return;
    fetchData();
  }, [isFetching]);

  return (
    <div >
      {listItems.map(listItem => (
        <div key={listItem.id}>
          <h3>{listItem.title}</h3>
          <p>{listItem.body}</p>
        </div>
      ))}
      {isFetching && <div>Loading more posts...</div>}
    </div>
  );
};

export default List;
