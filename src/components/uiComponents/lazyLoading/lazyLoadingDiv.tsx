import React, { useState, useEffect, useRef } from "react";

interface ListItem {
  id: number;
  title: string;
  body: string;
}

function List() {
  const [listItems, setListItems] = useState<ListItem[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const loader = useRef<HTMLDivElement>(null);

  const handleObserver = (entities: IntersectionObserverEntry[]) => {
    const target = entities[0];
    if (target.isIntersecting) {
      setIsLoading(true);
    }
  };

  useEffect(() => {
    const options = {
      root: null,
      rootMargin: "0px",
      threshold: 0.3,
    };

    const observer = new IntersectionObserver(handleObserver, options);
    const target = loader.current;
    if (target) {
      observer.observe(target);
    }
    return () => observer.disconnect();
  }, []);

  useEffect(() => {
    if (!isLoading) return;
    fetchData();
  }, [isLoading]);

  const fetchData = async () => {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=10`
    );
    const data: ListItem[] = await response.json();
    setPage(page + 1);
    setListItems(prevState => [...prevState, ...data]);
    setIsLoading(false);
  };

  return (
    <div style={{ height: "400px", overflow: "auto" }}>
      {listItems.map(listItem => (
        <div key={listItem.id}>
          <h3>{listItem.title}</h3>
          <p>{listItem.body}</p>
        </div>
      ))}
      <div ref={loader} style={{ height: "20px" }}>
        {isLoading && <p>Loading more items...</p>}
      </div>
    </div>
  );
}

export default List;
