import React, { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import './orderManagement.css'
import { reorderInventory,saleInventory } from "../../action/inventoryAction";

export default function OrderManagement() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [sellProduct, setSellProduct] = useState({
        menProduct: 0,
        womenProduct: 0,

    });

    const handleData = (e: any) => {
        setSellProduct({
            ...sellProduct, [e.target.name]: e.target.value
        })
    }

    const updateProduct = () => {
        dispatch(saleInventory('menProduct',sellProduct.menProduct));
        dispatch(saleInventory('womenProduct',sellProduct.womenProduct));
        navigate('/');

    }

    return (
        <div className="sellProduct">
            <>
                <form className="sellProduct-form" >
                    <label >Sell men product</label><br />
                    <input className="sellProduct-text"
                        type="number"
                        name="menProduct"
                        onChange={handleData}
                        value={sellProduct.menProduct}
                    /><br />

                    <label>Sell women product</label><br />
                    <input className="sellProduct-text"
                        type="number"
                        name="womenProduct"
                        onChange={handleData}
                        value={sellProduct.womenProduct}
                    />
                    <br />

                    <div className="button-layout">
                        <button className="sellProduct-submit" onClick={updateProduct}>Sell product</button>
                        <input className="sellProduct-back" onClick={() => navigate('/')} type="submit" value="Back" />

                    </div>

                </form>

            </>

        </div>
    );

}