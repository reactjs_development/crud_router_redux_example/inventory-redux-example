import React, { useState, useEffect } from "react";
import './orderView.css'
import { useSelector, useDispatch } from "react-redux"
// import { GET_INVENTORY_STOCK,SALE_INVENTORY_STOCK } from "../../action/actionTypes";

export default function OrderView() {
    const stocks = useSelector((state: any) => state.inventory);
    console.log(stocks);

    return (
        <div className="orderView">
            <table className="table">
                <tr>
                    <th>Products</th>
                    <th>Stocks available</th>

                </tr>
                <tr >
                    <td>Men Products</td>
                    <td>{stocks?.menProduct}</td>

                </tr>

                <tr >
                    <td>Women Products</td>
                    <td>{stocks?.womenProduct}</td>
                </tr>


            </table>
        </div>
    );

}