import React, { useState, useEffect } from "react";
import './userDetailsView.css'
import { useSelector, useDispatch } from "react-redux"
import { getUserDetails } from "../../action/userDetails";

interface userProp {
    isLoading: false,
    isFailed: false,
    userDetails: any,
};

export default function UserDetailsView() {
    const { isLoading, isFailed, userDetails }: userProp = useSelector((state: any) => state.users);

    const despatch = useDispatch();
    const [data, setData] = useState([]);

    useEffect(() => {
        despatch(getUserDetails() as any)
    }, []);

    useEffect(() => {
        setData(userDetails)
    }, [userDetails]);

    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage, setPostsPerPage] = useState(3);
    const [search, setSearch] = useState("");

    // Get current posts
    const indexOfLastPost = currentPage * postsPerPage;  // 1* 3 = 3 inital value
    const indexOfFirstPost = indexOfLastPost - postsPerPage; // 3 -3 = 0
    const currentPosts = data?.slice(Number(indexOfFirstPost), Number(indexOfLastPost));
    console.log("currentPosts",currentPosts)

    const handleClick = (page: any) => setCurrentPage(page);


    const handleChange = (e: any) => {
        setPostsPerPage(e.target.value);
        setCurrentPage(1);
    };

    const handleSearch = (e: any) => {
        setSearch(e.target.value);
        setCurrentPage(1);
    };

    // Filtering the data based on the search input:
    const filteredData = currentPosts?.filter((user: any) =>
        user.name.toLowerCase().includes(search.toLowerCase())
    );

    useEffect(() => {
        setData(userDetails)
    }, [userDetails]);
    
    console.log("filteredData",filteredData)

    return (
        <div className="orderView">

            {isLoading && <h2>Loading.................</h2>}
            {isFailed && <h2>something went wrong</h2>}

            {filteredData &&
                <>
                    <div>
                        <label>select number of data per page </label>
                        <select value={postsPerPage} onChange={handleChange}>
                            <option value={3}>default</option>
                            <option value={2}>2</option>
                            <option value={5}>5</option>
                        </select>
                    </div>
                    <div>
                <input
                    className="search-input"
                    type="text"
                    placeholder="Search by title"
                    value={search}
                    // onChange={(event) => setSearch(event.target.value) }
                    onChange={handleSearch}

                />
            </div>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>

                            </tr>
                        </thead>
                        {filteredData?.map((user: any, index) => (
                            <tbody key={index}>
                                <tr >
                                    <td>{user?.name}</td>
                                    <td>{user?.email}</td>

                                </tr>
                            </tbody>
                        ))}

                    </table>
                    <div className="btn">

                        <h3>{currentPage}-{currentPosts.length} of {data.length}</h3>
                        <div className="btn">
                            <button
                                className="pagination-button"
                                disabled={currentPage === 1}
                                onClick={() => handleClick(currentPage - 1)}
                            >
                                {"<"}
                            </button>
                            <button
                                className="pagination-button"
                                disabled={currentPage === Math.ceil(data.length / postsPerPage)}
                                onClick={() => handleClick(currentPage + 1)}
                            >
                                {">"}
                            </button>
                        </div>

                    </div>

                </>
            }

        </div>
    );

}